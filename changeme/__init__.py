"""Top-level package for CHANGEME."""
from ._version import get_versions

__version__ = get_versions()['version']
del get_versions
__author__ = """CHANGEME"""
__email__ = 'CHANGEME'
